package docker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import docker.services.ComposeRunner;
import docker.services.FileManipulator;
import docker.services.impl.ComposeRunnerImpl;
import docker.services.impl.FileManipulatorImpl;

public class ContainerRunner {

	public static void main(String[] args) throws IOException {

		final String inputFileName = "input/docker-compose.yml";
		final String microservicesFileName = "input/microservices.txt";
		final String activeMicroservicesFileName = "input/active-microservices.txt";
		final String outputFileName = "target/docker-compose.yml";
		
		final FileManipulator fileManipulator = new FileManipulatorImpl();
		final ComposeRunner composeRunner = new ComposeRunnerImpl();

		final BufferedReader reader = fileManipulator.readInputFile(inputFileName);
		final BufferedWriter writer = fileManipulator.createOutputFile(outputFileName);

		final List<String> microservices = fileManipulator.readMicroservicesFile(microservicesFileName);
		final List<String> activeMicroservices = fileManipulator.readMicroservicesFile(activeMicroservicesFileName);

		final StringBuilder output = fileManipulator.writeDockerCompose(reader, microservices, activeMicroservices);

		writer.write(output.toString());
		writer.close();

		composeRunner.runCompose(outputFileName);

	}

}
