package docker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import docker.services.FileManipulator;
import docker.services.impl.FileManipulatorImpl;

public class ActiveMicroserviceRetriever {

	public static void main(String[] args) throws IOException {

		final String logsBeforeTest = "target/logs_before.txt";
		final String logsAfterTest = "target/logs_after.txt";
		final String resultLogs = "target/result_logs.txt";

		List<String> microservices = new ArrayList<String>();
		
		microservices.add("1d1da8233542");
		microservices.add("d959213238fe");

		final FileManipulator fileManipulator = new FileManipulatorImpl();

		BufferedWriter  writer = fileManipulator.createOutputFile(resultLogs);

		final StringBuilder resultingLogs = fileManipulator.getLogsFromTests(logsBeforeTest, logsAfterTest);

		writer.write(resultingLogs.toString());
		writer.close();

		final File inputFile = new File(resultLogs);
		final String decoded = URLDecoder.decode(inputFile.getAbsolutePath(), "UTF-8");
		final BufferedReader reader = new BufferedReader(new FileReader(decoded));

		String line;

		StringBuilder touchedMicroservices = new StringBuilder();

		while ((line = reader.readLine()) != null) {
			for (String microservice : microservices) {

				if (line.contains(microservice)) {
					touchedMicroservices.append(microservice + "\n");
				}
			}

		}

		 writer = fileManipulator.createOutputFile("target/touchedMicroservices");

		writer.write(touchedMicroservices.toString());
		writer.close();

	}

}
